#!/bin/bash

docker image build -t vargab95/joplin-cli .


if [ "$#" -eq 1 ]; then
    if [[ "$1" == "--push" ]]; then
        docker image push vargab95/joplin-cli
    fi

    exit 1
fi

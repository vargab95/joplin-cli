# Joplin CLI

A docker container to run Joplin from command line.

## Reuirements

docker must be installed.

## Setup

Run setup.sh, source your .bashrc, then the joplin-cli command should be
available.

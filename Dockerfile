FROM node:19.6

RUN apt update && apt install -y neovim
RUN npm install -y -g joplin

CMD ["joplin"]

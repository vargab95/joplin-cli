#!/bin/bash

echo "alias joplin-cli='docker container run -it --rm -v $HOME/.config/joplin-desktop:/home/joplin/.config/joplin:rw -u $(id -u ${USER}):$(id -g ${USER}) vargab95/joplin-cli'" >> ~/.bash_aliases
